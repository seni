/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#include <string.h>
#include "menu.hpp"
#include "engine.hpp"

Option::Option()
{
	this->id = 0;
	this->image[0] = 0;
	this->Simage = 0;
	this->event = 0;
	this->engine = 0;
}

Option::Option(Engine *e)
{
	this->id = 0;
	this->image[0] = 0;
	this->Simage = 0;
	this->event = 0;
	this->engine = e;
}

Option::Option(char *f, Engine *e)
{
	this->id = 0;
	this->engine = e;
	this->setImage(f);
	this->event = 0;
}

Option::Option(char *f, int i, Engine *e)
{
	this->setId(i);
	this->engine = e;
	this->setImage(f);
	this->event = 0;
}

int Option::getId(void)
{
	return this->id;
}

void Option::setId(int i)
{
	this->id = i;
}

int Option::setImage(char *f)
{
	char err[255];
	if(!this->engine)
		return MENU_NO_ENGINE;
	strcpy(this->image, f);
	this->Simage = SDL_LoadBMP(f);
	if(!this->Simage){
		strcpy(err, "Error: SDL_LoadBMP(f): ");
		strcat(err, SDL_GetError());
		this->engine->setError(err);
		return MENU_ERROR;
	}
	this->engine->setError((char *)"No hay errores", ERROR_NO_PRINT);
	return MENU_NO_ERROR;
}

void Option::setEvent(option_event oe)
{
	event = oe;
}

int Option::execEvent(SDL_Event *se, Engine *e, Menu *m, Option *o)
{
	if(!this->event)
		return OPTION_STAY;
	return this->event(se, e, m, o);
}

int Option::show(void)
{
	char err[255];
	if(!this->engine)
		return MENU_NO_ENGINE;
	if(!this->Simage){
		engine->setError((char *)"Error: No se asigno imagen");
		return MENU_ERROR;
	}
	if(this->engine->show(this->Simage, this->x, this->y, this->Simage->w, this->Simage->h) == ENGINE_ERROR){
		strcpy(err, "Error: Option->show()->engine->show(): ");
		strcat(err, SDL_GetError());
		this->engine->setError(err);
		return MENU_ERROR;
	}
// 	this->engine->update(this->x, this->y, this->Simage->w, this->Simage->h);
	engine->setError((char *)"No hay errores", ERROR_NO_PRINT);
	return MENU_NO_ERROR;
	
}

int Option::update(void)
{
	if(!engine)
		return MENU_NO_ENGINE;
	this->engine->update(this->x, this->y, this->Simage->w, this->Simage->h);
	return MENU_NO_ERROR;
}

void Option::setXY(int x, int y)
{
	this->x = x;
	this->y = y;
}

int Option::getX(void)
{
	return this->x;
}

int Option::getY(void)
{
	return this->y;
}

int Option::getH(void)
{
	return this->Simage ? this->Simage->h : 0;
}

int Option::getW(void)
{
	return this->Simage ? this->Simage->w : 0;
}

void Option::setEngine(Engine *e)
{
	this->engine = e;
}

Menu::Menu(void)
{
	int i;
	this->engine = 0;
	for(i = 0; i < MAX_OPTIONS; i++)
		this->options[i] = WITHOUT_OPTION;
	this->wallpaper[0] = 0;
}

Menu::Menu(Engine *e)
{
	int i;
	this->engine = e;
	for(i = 0; i < MAX_OPTIONS; i++)
		this->options[i] = WITHOUT_OPTION;
	this->wallpaper[0] = 0;
}

Menu::Menu(char *w, Engine *e)
{
	int i;
	this->engine = e;
	for(i = 0; i < MAX_OPTIONS; i++)
		this->options[i] = WITHOUT_OPTION;
	this->setWallpaper(w);
}

Menu::Menu(char *w, Option **o, int im, Engine *e)
{
	int i;
	this->engine = e;
	for(i = 0; i < im && i < MAX_OPTIONS; i++){
		this->options[i] = o[i];
	}
	for(;i < MAX_OPTIONS; i++)
		this->options[i] = WITHOUT_OPTION;
	this->setWallpaper(w);
}

int Menu::setWallpaper(char *w)
{
	char err[255];
	strcpy(this->wallpaper, w);
	this->Swallpaper = SDL_LoadBMP(w);
	if(!this->Swallpaper){
		strcpy(err, "Error: SDL_LoadBMP(w): ");
		strcat(err, SDL_GetError());
		this->engine->setError(err);
		return MENU_ERROR;
	}
	this->engine->setError((char *)"No hay errores", ERROR_NO_PRINT);
	return MENU_NO_ERROR;
}

int Menu::addOption(Option *o)
{
	int i;
	for(i = 0; i < MAX_OPTIONS; i++){
		if(this->options[i] == WITHOUT_OPTION){
			this->options[i] = o;
			return MENU_NO_ERROR;
		}
	}
	return OPTIONS_FULL;
}

Option *Menu::getOptionById(int oid)
{
	int i;
	for(i = 0; i < MAX_OPTIONS; i++){
		if(options[i] != WITHOUT_OPTION && options[i]->getId() == oid){
			return options[i];
		}
	}
	return 0;
}

Option *Menu::getOption(int i)
{
	if(i >= MAX_OPTIONS)
		return WITHOUT_OPTION;
	return this->options[i];
}

int Menu::removeOption(int oid)
{
	int i;
	for(i = 0; i < MAX_OPTIONS; i++){
		if(options[i] != WITHOUT_OPTION && options[i]->getId() == oid){
			options[i] = WITHOUT_OPTION;
			return MENU_NO_ERROR;
		}
	}
	return NO_OPTION;
}

int Menu::show()
{
	int i;
	char err[255];
	if(!this->engine)
		return MENU_NO_ENGINE;
	if(this->engine->show(this->Swallpaper, 0, 0, this->Swallpaper->w, this->Swallpaper->h) == ENGINE_ERROR){
		strcpy(err, "Error: Menu->show()->engine->show(): ");
		strcat(err, SDL_GetError());
		this->engine->setError(err);
		return MENU_ERROR;
	}
// 	this->engine->update(0, 0, 0, 0);
	for(i = 0; i < MAX_OPTIONS; ++i){
		if(this->options[i] != WITHOUT_OPTION){
			int r = this->options[i]->show();
			if(r != MENU_NO_ERROR)
				return r;
		}
	}
	return MENU_NO_ERROR;
}

int Menu::update(void)
{
	if(!engine)
		return MENU_NO_ENGINE;
	this->engine->update(0, 0, 0, 0);
	return MENU_NO_ERROR;
}

void Menu::setEngine(Engine *e)
{
	this->engine = e;
}
