/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#ifndef __VERSION_ENGINE_H__
#define __VERSION_ENGINE_H__

#define VERSION (0x000200)
#define GET_MAYORV(a) (a>>16)
#define GET_MINORV(a) ((a>>8)&0x00FF)
#define GET_ALPHABETA(a) (!(a&0xF)?"alpha":((a&0xF) == 1?"beta":""))

#endif
