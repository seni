/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#ifndef __ENGINE__H_SENI
#define __ENGINE__H_SENI

#include <SDL/SDL.h>

#define ENGINE_NO_ERROR (0)
#define ENGINE_ERROR (ENGINE_NO_ERROR + 1)

#define MAX_EVENTS (6)
#define MAX_MENUHIST (20)

#define ERROR_NO_PRINT (0)
#define ERROR_PRINT (ERROR_NO_PRINT + 1)

#define EVENT_QUIT (0)
#define EVENT_MOUSEMOTION (EVENT_QUIT + 1)
#define EVENT_MOUSEBUTTON (EVENT_QUIT + 2)
#define EVENT_KEYBOARD (EVENT_QUIT + 3)
#define EVENT_RESIZE (EVENT_QUIT + 4)
#define EVENT_DEFAULT (EVENT_QUIT + 5)

class Engine;

#include "menu.hpp"

typedef void (*engine_event)(SDL_Event *, Engine *);
typedef void (*quit_event)(void);

class Engine{
	public:
		Engine(void);
		int initVideo(int, int, int, int);
		void quit(void);
		char *getError(void);
		void setError(char *);
		void setError(char *, int);
		void setEvent(int, engine_event);
		void setEventInAll(engine_event);
		void setOnQuit(quit_event);
		void startEvents(void);
		void update(void);
		void update(int, int, int, int);
		int showMenu(Menu *);
		int show(SDL_Surface *, int, int, int, int);
		int goBackMenu(void);
		int getVersionInt(void);
		void getVersion(char *);
	private:
		int showMenu(int i);
		void optionClick(SDL_Event *);
		char engineError[255];
		engine_event events[MAX_EVENTS];
		quit_event onquit;
		SDL_Surface *screen;
		int menu;
		Menu *menuhist[MAX_MENUHIST];
};

#endif
