/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#ifndef __MENU_ENGINE__H_SENI
#define __MENU_ENGINE__H_SENI

#define WITHOUT_OPTION (0)

#define MAX_OPTIONS (20)

#define MENU_NO_ERROR (0)
#define MENU_ERROR (MENU_NO_ERROR + 1)
#define OPTIONS_FULL (MENU_NO_ERROR + 2)
#define NO_OPTION (MENU_NO_ERROR + 3)
#define MENU_NO_ENGINE (MENU_NO_ERROR + 4)

#define OPTION_RETURN (0)
#define OPTION_STAY (OPTION_RETURN + 1)

class Option;
class Menu;

#include "engine.hpp"

typedef int (*option_event)(SDL_Event *, Engine *, Menu *, Option *);

class Option{
	public:
		Option(void);
		Option(Engine *);
		Option(char *, Engine *);
		Option(char *, int, Engine *);
		int getId(void);
		void setId(int);
		int setImage(char *);
		void setEvent(option_event);
		int execEvent(SDL_Event *, Engine *, Menu *, Option *);
		int show(void);
		int update(void);
		void setEngine(Engine *);
		void setXY(int, int);
		int getH(void);
		int getW(void);
		int getX(void);
		int getY(void);
	private:
		int id;
		char image[255];
		int x;
		int y;
		SDL_Surface *Simage;
		option_event event;
		Engine *engine;
};

class Menu{
	public:
		Menu(void);
		Menu(Engine *);
		Menu(char *, Engine *);
		Menu(char *, Option **, int, Engine *);
		int setWallpaper(char *);
		int addOption(Option *);
		Option *getOption(int);
		Option *getOptionById(int);
		int removeOption(int);
		int show(void);
		int update(void);
		void setEngine(Engine *);
	private:
		Option *options[MAX_OPTIONS];
		char wallpaper[255];
		SDL_Surface *Swallpaper;
		SDL_Surface *Smenu;
		Engine *engine;
};

#endif
