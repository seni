/*
Copyright SSW Team 2010
*/

/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#include <SDL/SDL.h>
#include <string.h>
#include <stdio.h>
#include "engine.hpp"
#include "menu.hpp"
#include "engine_version.h"

void defaultEvent(SDL_Event *, Engine *);

int Engine::getVersionInt(void)
{
	return VERSION;
}

void Engine::getVersion(char *o)
{
	sprintf(o, "%d.%d%s", GET_MAYORV(VERSION), GET_MINORV(VERSION), GET_ALPHABETA(VERSION));
}

int Engine::initVideo(int resx, int resy, int bitres, int flags)
{
	char err[255];
	if(SDL_Init(SDL_INIT_TIMER | SDL_INIT_AUDIO | SDL_INIT_VIDEO)){
		strcpy(err, "Error: SDL_Init(SDL_INIT_TIMER | SDL_INIT_AUDIO | SDL_INIT_VIDEO): ");
		strcat(err, SDL_GetError());
		this->setError(err);
		return ENGINE_ERROR;
	}
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, bitres);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	this->screen = SDL_SetVideoMode(resx, resy, bitres, flags);
	if(!screen){
		strcpy(err, "Error: SDL_SetVideoMode(sc.resx, sc.resy, sc.bitres, sc.sdlflags): ");
		strcat(err, SDL_GetError());
		this->setError(err);
		return ENGINE_ERROR;
	}
	this->setError((char *)"No hay errores", ERROR_NO_PRINT);
	return ENGINE_NO_ERROR;
}

void Engine::quit(void)
{
	SDL_Quit();
	if(this->onquit != 0)
		this->onquit();
}

void Engine::setOnQuit(quit_event qe)
{
	this->onquit = qe;
}

char *Engine::getError()
{
	return this->engineError;
}

void Engine::setError(char *e)
{
	this->setError(e, ERROR_PRINT);
}

void Engine::setError(char *e, int i)
{
	strcpy(this->engineError, e);
	if(i == ERROR_PRINT)
		fprintf(stderr, "%s\n", this->engineError);
}

Engine::Engine(void)
{
	this->engineError[0] = 0;
	this->setEventInAll(defaultEvent);
	this->onquit = 0;
	this->menu = -1;
}

void Engine::setEvent(int i, engine_event ee)
{
	if(!ee)
		ee = defaultEvent;
	if(i < MAX_EVENTS)
		this->events[i] = ee;
}

void Engine::setEventInAll(engine_event ee)
{
	int i;
	if(!ee)
		ee = defaultEvent;
	for(i = 0; i < MAX_EVENTS; i++)
		this->events[i] = ee;
}

void Engine::startEvents(void)
{
	SDL_Event ev;
	while(SDL_WaitEvent(&ev)){
		switch(ev.type){
			case SDL_QUIT:
				this->events[EVENT_QUIT](&ev, this);
				break;
			case SDL_MOUSEMOTION:
				this->events[EVENT_MOUSEMOTION](&ev, this);
				break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				this->events[EVENT_MOUSEBUTTON](&ev, this);
				this->optionClick(&ev);
				break;
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				this->events[EVENT_KEYBOARD](&ev, this);
				break;
			case SDL_VIDEORESIZE:
				this->events[EVENT_RESIZE](&ev, this);
				break;
			default:
				this->events[EVENT_DEFAULT](&ev, this);
		}
	}
}

void Engine::optionClick(SDL_Event *ev)
{
	int i;
	if(ev->button.button == SDL_BUTTON_LEFT && ev->button.type == SDL_MOUSEBUTTONDOWN && this->menu > -1){
		for(i = 0; i < MAX_OPTIONS; ++i){
			Option *o = this->menuhist[this->menu]->getOption(i);
			if(o != WITHOUT_OPTION){
				int x = ev->button.x, y = ev->button.y;
				if(x >= o->getX() && x <= o->getX() + o->getW()
				  && y >= o->getY() && y <= o->getY() + o->getH()){
					if(o->execEvent(ev, this, this->menuhist[this->menu], o) == OPTION_RETURN)
						this->goBackMenu();
				  }
			}
		}
	}
}

void Engine::update(void)
{
	SDL_GL_SwapBuffers();
}

void Engine::update(int xi, int yi, int xf, int yf)
{
	SDL_UpdateRect(screen, xi, yi, xf, yf);
	this->update();
}

int Engine::show(SDL_Surface *s, int x, int y, int w, int h)
{
	SDL_Rect dest;
	if(!s){
		return ENGINE_ERROR;
	}
	dest.x = x;
	dest.y = y;
	dest.w = w;
	dest.h = h;
	if(SDL_BlitSurface(s, NULL, screen, &dest))
		return ENGINE_ERROR;
	return ENGINE_NO_ERROR;
}

int Engine::showMenu(Menu *m)
{
	this->menuhist[++(this->menu)] = m;
	return this->showMenu(this->menu);
}

int Engine::showMenu(int i)
{
	char err[255];
	Menu *m = this->menuhist[i];
	if(m->show() == MENU_ERROR){
		strcpy(err, "Error: m->show(screen): ");
		strcat(err, SDL_GetError());
		this->setError(err);
		return ENGINE_ERROR;
	}
	m->update();
	this->setError((char *)"No hay errores", ERROR_NO_PRINT);
	return ENGINE_NO_ERROR;
}

int Engine::goBackMenu(void)
{
	if(this->menu <= 0){
		this->setError((char *)"Error: Engine->goBackMenu(): No hay menues anteriores.");
		return ENGINE_ERROR;
	}
	return this->showMenu(--(this->menu));
}

void defaultEvent(SDL_Event *e, Engine *engine)
{
	switch(e->type){
		case SDL_QUIT:
			engine->quit();
			break;
		case SDL_VIDEOEXPOSE:
			engine->update();
			break;
	}
}
