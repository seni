#Makefile para SENI
#SSW Team

GPP=g++
DIROUT=build
MACHINE=$(shell uname -m)
FILEOUT=seni
OUT=$(DIROUT)/$(MACHINE)/$(FILEOUT)

ENGINE_DIR=engine/
SRC_DIR=src/
RES_DIR=res/

SRC=$(SRC_DIR)*.cpp
SRC_ALL=$(SRC_DIR)*.cpp $(SRC_DIR)*.hpp $(SRC_DIR)*.h

ENGINE_ALL=$(ENGINE_DIR)*.cpp $(ENGINE_DIR)*.hpp
ENGINE=$(ENGINE_DIR)*.cpp

ENGINE_OUT=$(ENGINE_DIR)libseniengine.a

.PHONY: all clean engine seni clean-all clean-engine clean-seni clean-build stats

all: engine seni

seni: $(OUT)

$(OUT): $(DIROUT)/$(MACHINE) $(SRC) $(ENGINE_OUT)
	@echo
	@echo -e "\t\e[1;36mMaking SENI\e[0m"
	@cd $(SRC_DIR) && make seni
	
$(DIROUT)/$(MACHINE):
	@echo
	mkdir -p $(DIROUT)/$(MACHINE)
	
engine: $(ENGINE_OUT)

$(ENGINE_OUT): $(ENGINE)
	@echo
	@echo -e "\t\e[1;36mMaking Engine\e[0m"
	@cd $(ENGINE_DIR) && make engine
	
clean: clean-all

clean-all: clean-engine clean-seni clean-build
	- rm *~

clean-build:
	- rm -r $(DIROUT)
	- rm seni

clean-engine:
	cd $(ENGINE_DIR) && make clean
	
clean-seni:
	cd $(SRC_DIR) && make clean

stats: $(ENGINE_OUT) $(ENGINE_ALL) $(OUT) $(SRC_ALL)
	@echo -ne "\e[1;33mLineas Engine\n\t\e[1;34m"
	@wc -l $(ENGINE_ALL) | grep -w total
	@echo -e "\e[0m"
	@echo -ne "\e[1;33mBytes Engine\n\t\e[1;34m"
	@wc -c $(ENGINE_OUT)
	@echo -e "\e[0m"
	@echo -ne "\e[1;33mLineas SENI\n\t\e[1;34m"
	@wc -l $(SRC_ALL) | grep -w total
	@echo -e "\e[0m"
	@echo -ne "\e[1;33mBytes SENI\n\t\e[1;34m"
	@wc -c $(OUT)
	@echo -e "\e[0m"
