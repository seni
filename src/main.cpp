/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#include <iostream>
#include <string.h>

#include "all.hpp"
#include "engine.hpp"
#include "seni_version.h"

using namespace std;

/*hagamosnos los h4x0r5 y dejemos mensajes dentro del codigo y del ejecutable! XD
dejen ustedes el que quieran :)*/
/*ahora descubr� q esto es una especie de "huevo de pascua"*/
char ___[] = "And You Just Laugh, HaHa, Bitch!\0\0" /*Megadeth - In My Darkest Hour*/
	"Es verdad: nosotros amamos la vida no porque estemos habituados a vivir, sino porque "
	"estamos habituados a amar. "
	"Siempre hay algo de demencia en el amor. Pero siempre hay tambi�n algo de raz�n en la demencia.\0\0" /*Friedrich Nietzsche en su libro "Asi hablo Zaratustra"*/
	"SSW Team - Shadow - Ramc - Ni0 - [p3ll3]\0\0\0";

Args *args = 0;
Config *conf = 0;
Engine *engine = 0;

char *checkear[CHECKEAR_T] = CHECKEAR;
char msg[255];

int doArgs(char **);
void printHelp(void);
void printVer(void);
int initVideo(Config *);
void startitup(void);

int main(int argc, char *argv[])
{
	msg[0] = 0;
	args = new Args(argc, argv);
	conf = new Config();
	
	cout << "\n/*--------------------------*/\n\tSENI\nSearch for Extra Nibiru Intelligence\n\nSSW Team\n";
	cout << "\n";
	
	if(doArgs(argv) != SALIR){
		startitup();
	}
	terminar();
	return 0;
}

void terminar(void)
{
	salir(msg);
	if(args != 0)
		delete(args);
	if(conf != 0)
		delete(conf);
	if(engine != 0)
		delete(engine);
	exit(0);
}

int doArgs(char **argv)
{
	sConf sc;
	int r, rr;
	char cp[50];
	
	r = args->checkArgs(checkear, CHECKEAR_T);
	conf->setDefault();
	conf->getConfig(&sc);
	
	if(r){
		cout << "\n\tError: Argumento no valido (#" << r << ") : " << argv[r] << "\n\nEjecute " << argv[0] << " -h";
		return SALIR;
	}
	if(args->getArg("-h") > 0 || args->getArg("--help") > 0){
		printHelp();
		return SALIR;
	}
	if(args->getArg("-v") > 0 || args->getArg("--version") > 0){
		printVer();
		return SALIR;
	}
	if(args->getArg("--errormode") > 0){
		conf->setErrorMode();
		conf->getConfig(&sc);
	}
	if(args->getArg("--default") > 0){
		conf->setDefault();
		conf->getConfig(&sc);
		/*si, se repite el codigo (primeras lineas de la funcion doArgs(), 
		pero es por si en algun momento no se carga por default el programa*/
	}
	if(args->getArg("-w") > 0 || args->getArg("--windowed") > 0)
		sc.sdlflags &= ~SDL_FULLSCREEN;
	if(args->getArg("--fullscreen") > 0)
		sc.sdlflags |= SDL_FULLSCREEN;
	if(args->getArg("--noopenglblit") > 0)
		sc.sdlflags &= ~SDL_OPENGLBLIT;
	if(args->getArg("-r") > 0){
		if(args->getMod("-r", cp) == -1){
			cout << "\n\tError: Se esperaba una resolucion en -r\n\tejemplo, -r 800x600";
			return SALIR;
		}
		rr = conf->doRes(cp, &sc.resx, &sc.resy);
		if(rr == -1){
			cout << "\n\tError: Se esperaba un \'X\' en la resoluci�n indicada: " << cp << "\n\tejemplo, -r 800x600";
			return SALIR;
		}
		if(rr){
			cout << "\n\tError: Caracter #" << rr << " no soportado en la resoluci�n indicada: " << cp;
			return SALIR;
		}
	}
	if(args->getArg("--resolucion") > 0){
		if(args->getMod("--resolucion", cp) == -1){
			cout << "\n\tError: Se esperaba una resolucion en --resolucion\n\tejemplo, --resulucion=800x600";
			return SALIR;
		}
		rr = conf->doRes(cp, &sc.resx, &sc.resy);
		if(rr == -1){
			cout << "\n\tError: Se esperaba un \'X\' en la resoluci�n indicada: " << cp << "\n\tejemplo, --resulucion=800x600";
			return SALIR;
		}
		if(rr){
			cout << "\n\tError: Caracter no soportado en la resoluci�n indicada (#"<< rr << "): " << cp;
			return SALIR;
		}
	}
	if(args->getArg("--bits") > 0){
		if(args->getMod("--bits", cp) == -1){
			cout << "\n\tError: Se esperaba una resolucion de bits en --bits\n\tejemplo, --bits=16";
			return SALIR;
		}
		if(!strcmp(cp, "32"))
			sc.bitres=32;
		else if(!strcmp(cp, "24"))
			sc.bitres=24;
		else if(!strcmp(cp, "16"))
			sc.bitres=16;
		else{
			cout << "\n\tError: Solo se permite 32, 24 o 16 bits en la resolucion de bits\n\tejemplo, --bits=16";
			return SALIR;
		}
	}
	conf->setConfig(&sc);
	return CONTINUAR;
}

void printHelp(void)
{
	cout << HELP_TEXT;
}

void printVer(void)
{
	Engine *e = new Engine();
	char v[100];
	printf("Version: %d.%d%s\n", GET_MAYORV(VERSION), GET_MINORV(VERSION), GET_ALPHABETA(VERSION));
	e->getVersion(v);
	printf("Version Engine: %s\n\n", v);
	// 	cout << "Version: " VERSION "\n\n";
}

void salir(const char *m)
{
	cout << "\nTerminando SENI";
	if(m && *m != 0)
		cout << "\n\tMensaje: " << m;
	else
		cout << " sin problemas";
	cout << "\n\nSSW Team\n\n/*--------------------------*/\n";
}

void startitup(void)
{
	sConf sc;
	engine = new Engine();
	conf->getConfig(&sc);
	if(engine->initVideo(sc.resx, sc.resy, sc.bitres, sc.sdlflags) == ENGINE_ERROR){
		strcpy(msg, engine->getError());
		return;
	}
	engine->setOnQuit(terminar);
	engine->setEvent(EVENT_KEYBOARD, keyboardevent);
	Menu *m = new Menu(engine);
	if(m->setWallpaper((char*)"res/logo.bmp") == MENU_ERROR){
		strcpy(msg, engine->getError());
		return;
	}
	Option *o = new Option((char *)"res/menu.bmp", engine);
	o->setXY(30,30);
	o->setEvent(exit_event);
	m->addOption(o);
	if(engine->showMenu(m) == ENGINE_ERROR){
		strcpy(msg, engine->getError());
		return;
	}
	engine->startEvents();
}
