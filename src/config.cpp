/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include "all.hpp"

Config::Config(void)
{
}

Config::Config(const sConf *s)
{
	this->setConfig(s);
}

void Config::setDefault(void)
{
	this->conf.resx = 1024;
	this->conf.resy = 768;
	this->conf.bitres = 24;
	this->conf.sdlflags = SDL_FULLSCREEN | SDL_DOUBLEBUF | SDL_HWSURFACE | SDL_OPENGLBLIT;
}

void Config::setErrorMode(void)
{
	this->conf.resx = 640;
	this->conf.resy = 480;
	this->conf.bitres = 16;
	this->conf.sdlflags = SDL_SWSURFACE;
}

void Config::setConfig(const sConf *s)
{
	this->conf.sdlflags = s->sdlflags;
	this->conf.resx = s->resx;
	this->conf.resy = s->resy;
	this->conf.bitres = s->bitres;
}

void Config::getConfig(sConf *s)
{
	s->sdlflags = this->conf.sdlflags;
	s->resx = this->conf.resx;
	s->resy = this->conf.resy;
	s->bitres = this->conf.bitres;
}

int Config::saveConfig(const char *n)
{
	FILE *f;
	f = fopen(n, "wb");
	if(!f)
		return CONFIG_FILE_ERROR;
	fwrite(&conf, sizeof(sConf), 1, f);
	fclose(f);
	return CONFIG_NO_ERROR;
}

int Config::loadConfig(const char *n)
{
	FILE *f;
	f = fopen(n, "rb");
	if(!f)
		return CONFIG_FILE_ERROR;
	fread(&conf, sizeof(sConf), 1, f);
	fclose(f);
	return CONFIG_NO_ERROR;
}

int Config::doRes(const char *r, int *x, int *y)
{
	char *c;
	int i;
	c = (char *)strchr(r, 'x');
	if(!c)
		c = (char *)strchr(r, 'X');
	if(!c)
		return -1;
	for(i = 0; r[i]; i++){
		if((r[i] < '0' || r[i] > '9') && r+i != c)
			return i+1;
	}
	*c = 0;
	*x = atoi(r);
	*y = atoi(c+1);
	return 0;
}
