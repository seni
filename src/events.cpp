/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#include "all.hpp"

void keyboardevent(SDL_Event *ev, Engine *e)
{
	if(ev->key.type == SDL_KEYUP && ev->key.keysym.sym == SDLK_ESCAPE)
		e->quit();
}

int exit_event(SDL_Event *se, Engine *e, Menu *m, Option *o)
{
	e->quit();
	return OPTION_RETURN;
}
