/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#ifndef __CONFIG_H__SENI
#define __CONFIG_H__SENI

#define CONFIG_NO_ERROR 0
#define CONFIG_FILE_ERROR 1

typedef struct {
	int resx;
	int resy;
	int bitres;
	int sdlflags;
}sConf;

class Config{
	public:
		Config(void);
		Config(const sConf *);
		void setDefault(void);
		void setErrorMode(void);
		int loadConfig(const char *n);
		int saveConfig(const char *n);
		void getConfig(sConf *);
		void setConfig(const sConf*);
		int doRes(const char *, int *, int *); 
	private:
		sConf conf;
};

#endif
