/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#include <string.h>
#include "all.hpp"

Args::Args(int argc, char **argv)
{
	this->argc = argc;
	this->argv = argv;
}

int Args::getArg(const char *in)
{
	int i;
	for(i = 0; i != argc; i++){
		if(!strncmp(argv[i], in, strlen(in))){
			return i;
		}
	}
	return -1;
}

/*
obtiene el modificador
--res=800x600

800x600 es el modificador, por ejemplo
*/
int Args::getMod(const char *i, char *o)
{
	int r;
	r = getArg(i);
	if(i[1] == '-'){
		char *mod = strchr(argv[r], '=');
		if(!mod)
			return -1;
		strcpy(o, ++mod);
		return ++r;
	}
	if(r >= 0 && r < (argc - 1)){
		r++;
		strcpy(o, argv[r]);
		return r;
	}
	return -1;
}

int Args::checkArgs(char **a, int ta)
{
	int i, ia, len;
	char *c;
	for(i = 1; i < this->argc; i++){
		for(ia = 0; ia < ta; ia++){
			if(argv[i][0] != '-')
				break;
			len = 250;
			if(argv[i][1] == '-'){
				c = strchr(argv[i], '=');
				if(c)
					len = c - argv[i];
			}
			if(!strncmp(argv[i], a[ia], len))
				break;
		}
		if(ia == ta)
			return i;
	}
	return 0;
}
