/*SENI, Search for Extra Nibiru Intelligence*/

/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
	Copyright SSW Team 2010
*/

#ifndef __MAIN_H__SENI
#define __MAIN_H__SENI

#define HELP_TEXT ("\n\tAYUDA\n\n"\
	"-h , --help\n\t\tMuestra la ayuda\n"\
	"-v , --version\n\t\tMuestra la version\n"\
	"-w , --windowed\n\t\tSe ejecuta en modo ventana\n"\
	"--fullscreen\n\t\tSe ejecuta en pantalla completa\n"\
	"-r RES , --resolucion=RES\n\t\tse ejecuta en la resolucion RES\n"\
	"\t\t-r 800x600 , --resolucion=800x600\n"\
	"--bits=BITS\n\t\tresolucion de bits (32bits - 24bits - 16bits)\n"\
	"\t\t--bits=32 , --bits=24, --bits=16\n"\
	"--errormode\n\t\tejecuta el programa en modo de error\n\t\t640x480, 16bits, windowed\n"\
	"\t\tla utilizacion de -w o -r o --bits al usar --errormode *afecta al programa*\n"\
	"\t\tes decir, ejecutar seni --errormode --bits=32 crea un modo error con 32bits\n"\
	"--default\n\t\tse ejecueta con las opciones por default\n"\
	"--noopenglblit\n\t\tdesactiva SDL_OPENGLBLIT (para cuando hay problemas iniciando GLX visual)\n"\
	"\n")

#define CHECKEAR {\
	(char*)"-h",\
	(char*)"--help",\
	(char*)"-v",\
	(char*)"--version",\
	(char*)"-w",\
	(char*)"--windowed",\
	(char*)"-r",\
	(char*)"--resolucion",\
	(char*)"--bits",\
	(char*)"--errormode",\
	(char*)"--fullscreen",\
	(char*)"--default",\
	(char*)"--noopenglblit"\
}

#define CHECKEAR_T 13

void salir(const char *);
void terminar(void);

#endif
